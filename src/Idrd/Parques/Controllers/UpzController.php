<?php

namespace Idrd\Parques\Controllers;

use App\Http\Controllers\Controller;
use Idrd\Parques\Repo\LocalizacionInterface;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Validator;

class UpzController extends Controller {
    private $repositorio;

	public function __construct(LocalizacionInterface $repositorio)
	{
		$this->repositorio = $repositorio;
	}

	public function obtenerPorLocalidad(Request $request, $id_localidad)
	{
		$resultados = $this->repositorio->upzPorLocalidad($id_localidad);

		return response()->json($resultados);
	}
}