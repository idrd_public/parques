<?php

namespace Idrd\Parques\Repo;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Config as Config;

class Material extends Eloquent {
	
	protected $table = 'material';
	protected $primaryKey = 'IdMaterial';
	protected $fillable = ['Material'];
	protected $connection = '';
	public $timestamps = false;

	public function __construct()
	{
		$this->connection = config('parques.conexion');
	}

}