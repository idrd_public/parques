<?php

namespace Idrd\Parques\Repo;

use Illuminate\Database\Eloquent\Model as Eloquent;

class ParqueDotacion extends Eloquent {
	
	protected $table = 'parquedotacion';
	protected $primaryKey = 'Id';
	protected $connection = 'i_fk_id_sector';
	public $timestamps = false;

	public function __construct()
	{
		$this->connection = config('parques.conexion');
	}

	public function dotacion()
	{
		return $this->belongsTo(config('parques.modelo_dotacion'), 'Id_Dotacion');
	}

	public function material()
	{
		return $this->belongsTo(config('parques.modelo_material'), 'MaterialPiso');
	}

	public function parque()
	{
		return $this->belongsTo(config('parques.modelo_parque'), 'Id_Parque');
	}

}