<?php

namespace Idrd\Parques\Repo;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Config as Config;

class Sector extends Eloquent {
	
	protected $table = 'sectores';
	protected $primaryKey = 'i_pk_id';
	protected $fillable = ['i_fk_id_parque','Sector','coordenada'];
	protected $connection = '';
	public $timestamps = false;

	public function __construct()
	{
		$this->connection = config('parques.conexion');
	}

	public function dotaciones()
	{
		return $this->hasMany(config('parques.modelo_parquedotacion'), 'i_fk_id_sector');
	}
}