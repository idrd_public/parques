<?php

namespace Idrd\Parques\Repo;

use Illuminate\Database\Eloquent\Model as Eloquent;

class OrganizacionesPromocion extends Eloquent {
	
	protected $table = 'OrganizacionesPromocion';
	protected $primaryKey = 'Id';
	protected $fillable = ['Organizacion'];
	protected $connection = '';
	public $timestamps = false;

	public function __construct()
	{
		$this->connection = config('parques.conexion');
	}
}