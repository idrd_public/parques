<?php

return array( 
  'conexion' => 'db_parques',
  
  'modelo_parque' => 'Idrd\Parques\Repo\Parque',
  'modelo_tipo' => 'Idrd\Parques\Repo\TipoParque',
  'modelo_material' => 'Idrd\Parques\Repo\Material',
  'modelo_sector' => 'Idrd\Parques\Repo\Sector',
  'modelo_localidad' => 'Idrd\Parques\Repo\Localidad',
  'modelo_upz' => 'Idrd\Parques\Repo\Upz',
  'modelo_barrio' => 'Idrd\Parques\Repo\Barrio',
  'modelo_dotacion' => 'Idrd\Parques\Repo\Dotacion',
  'modelo_parquedotacion' => 'Idrd\Parques\Repo\Parquedotacion',
  'modelo_organizaciones_promocion' => 'Idrd\Parques\Repo\OrganizacionesPromocion',
);